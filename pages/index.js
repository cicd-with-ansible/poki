import React, { Fragment } from 'react';
import Head from 'next/head';
import useSWR, { SWRConfig } from 'swr';
import CssBaseline from '@material-ui/core/CssBaseline';

import Hero from '../components/Hero';
import PokemonList from '../components/PokemonList';
import Navbar from '../components/Navbar';
import { LinearProgress } from '@material-ui/core';

const url = 'https://pokeapi.co/api/v2/pokedex/2/';
const fetcher = (...args) => fetch(...args).then((res) => res.json());

export async function getServerSideProps() {
  const data = await fetcher(url);
  return { props: { data } };
}

export default function Home(props) {
  const initialData = props.data;
  const { data, error } = useSWR(url, fetcher, initialData);
  return (
    <Fragment>
      <CssBaseline />
      <Head>
        <title>Poki</title>
        <link rel="icon" href="/favicon.ico" />
        <link href="/fonts/Dancing Script.ttf" rel="stylesheet" />
      </Head>
      <Navbar />
      <Hero />
      <main>
        {error ? <p>error</p> : ''}
        {!data ? <LinearProgress color="secondary" /> : ''}
        {data ? <PokemonList data={data} /> : ''}
      </main>
    </Fragment>
  );
}
