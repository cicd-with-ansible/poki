import React from 'react';

import { Container, Typography } from '@material-ui/core';

export default function Hero() {
  return (
    <Container maxWidth="sm">
      <Typography
        component="h1"
        variant="h2"
        align="center"
        color="textPrimary"
        gutterBottom
      >
        Poki
      </Typography>
      <Typography variant="h5" align="center" color="textSecondary" paragraph gutterBottom>
        Simple Pokedex that is made using Nextjs and Material-UI.
      </Typography>
    </Container>
  );
}
