import React, { useState } from 'react';
import {
  Card,
  Container,
  Grid,
  CardMedia,
  CardContent,
  CardActions,
  Button,
  Typography,
  makeStyles,
  Dialog,
  Slide,
  AppBar,
  Toolbar,
  IconButton,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';

import Search from './Search';
import PokemonDetail from './PokemonDetail';

const useStyles = makeStyles((theme) => ({
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
    backgroundSize: 'contain',
  },
  cardContent: {
    flexGrow: 1,
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function PokemonList(data) {
  const fetchedPokemon = data.data;
  const [open, setOpen] = useState(false);
  const [pokemon, setPokemon] = useState({
    entry_number: '',
    pokemon_species: { name: '', url: '' },
  });

  const handleClickOpen = (selectedPokemon) => {
    setOpen(true);
    setPokemon({});
    setPokemon(selectedPokemon.pokemon);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const classes = useStyles();

  return (
    <>
      <Container className={classes.cardGrid} maxWidth="md">
        <Grid container spacing={3}>
          <Search />
          {fetchedPokemon.pokemon_entries.map((p) => (
            <Grid item key={p.entry_number} xs={12} sm={6} md={4}>
              <Card className={classes.card}>
                <CardMedia
                  className={classes.cardMedia}
                  image={`https://pokeres.bastionbot.org/images/pokemon/${p.entry_number}.png`}
                  title="Image title"
                />
                <CardContent className={classes.cardContent}>
                  <Typography gutterBottom variant="h5" component="h2">
                    {p.entry_number} :{' '}
                    {p.pokemon_species.name.charAt(0).toUpperCase() +
                      p.pokemon_species.name.slice(1)}
                  </Typography>
                </CardContent>
                <CardActions>
                  <Button
                    size="large"
                    color="primary"
                    fullWidth
                    onClick={() =>
                      handleClickOpen({
                        pokemon: p,
                      })
                    }
                  >
                    View
                  </Button>
                </CardActions>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Container>

      <Dialog
        fullScreen
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={handleClose}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              {pokemon.pokemon_species.name.charAt(0).toUpperCase() +
                pokemon.pokemon_species.name.slice(1)}
            </Typography>
          </Toolbar>
          <PokemonDetail pokemon={pokemon} />
        </AppBar>
      </Dialog>
    </>
  );
}
