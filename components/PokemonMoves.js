import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Typography,
  Grid,
  Paper,
  GridList,
  GridListTile,
} from '@material-ui/core';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: 500,
    height: 450,
  },
}));

export default function PokemonMoves(data) {
  const { moves } = data.data;

  const classes = useStyles();
  let i = 0;
  return (
    <Box p={4}>
      <Typography variant="h5" align="left" gutterBottom>
        Base moves
      </Typography>
      {/* <per>{moves.splice(50,moves.length).length}</per> */}
      <Grid container>
        {moves
          .splice(moves.length - 0.10 * moves.length, moves.length)
          .map((m) => (
            <Grid item xs={12} md={6}>
              <List key={Math.floor(Math.random() * 100)}>
                <ListItem key={Math.floor(Math.random() * 100)}>
                  <ListItemAvatar>
                    <Avatar>{m.move.name.charAt(0).toUpperCase()}</Avatar>
                  </ListItemAvatar>
                  <ListItemText
                    primary={
                      m.move.name.charAt(0).toUpperCase() + m.move.name.slice(1)
                    }
                  />
                </ListItem>
              </List>
            </Grid>
          ))}
      </Grid>
    </Box>
  );
}
