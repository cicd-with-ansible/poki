import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Typography, Grid, Paper } from '@material-ui/core';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function PokemonBaseStat(data) {
  const { stats } = data.data;
  const classes = useStyles();

  return (
    <Box p={4}>
      <Typography variant="h5" align="left" gutterBottom>
        Base Stats
      </Typography>
      <Grid container>
        {stats.map((s) => (
          <Grid item xs={4} md={6}>
            <List >
              <ListItem key={Math.floor(Math.random() * 100)}>
                <ListItemAvatar>
                  <Avatar>{s.stat.name.charAt(0).toUpperCase()}</Avatar>
                </ListItemAvatar>
                <ListItemText
                  primary={
                    s.stat.name.charAt(0).toUpperCase() + s.stat.name.slice(1)
                  }
                  secondary={s.base_stat}
                />
              </ListItem>
            </List>
          </Grid>
        ))}
      </Grid>
    </Box>
  );
}
