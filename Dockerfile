FROM node:current-alpine
RUN addgroup -g 1010 poki \
  && adduser -h /home/pokiu -s /bin/ash -D -u 1011 -g poki pokiu

WORKDIR /home/pokiu/poki

COPY ./.next /home/pokiu/poki/.next
COPY ./package.json /home/pokiu/poki
COPY ./yarn.lock /home/pokiu/poki
COPY ./LICENSE /home/pokiu/poki
RUN yarn install --prod

EXPOSE 3000
RUN export NODE_ENV=production
CMD ["yarn", "start"]