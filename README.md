This is Simple Application that is made with [Next.js](https://nextjs.org/) that interface with [pokiapi](https://pokiapi.co) and [pokers](https://pokeres.bastionbot.org/) to get the Pokemon data and view it Next.js . 
## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```
